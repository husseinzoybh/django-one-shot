from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.urls import reverse_lazy, reverse


# Create your views here.
class TodoListview(ListView):
    model = TodoList
    context_object_name = "todolist"
    template_name = "todos/todo_list_list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/details.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list_list")

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.pk})

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(TodoListCreateView, self).get_form_kwargs(
            *args, **kwargs
        )
        return kwargs


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.pk})


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/create_item.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.list.pk})

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(TodoItemCreateView, self).get_form_kwargs(
            *args, **kwargs
        )
        return kwargs


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/update_item.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse("todo_list_detail", kwargs={"pk": self.object.list.pk})
